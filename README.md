This is the readme for yawcl-c.

What is yawcl-c?

yawcl-c (Yet Another Wrapper for OpenCL in C) is the tentative name for a new OpenCL wrapper written entirely in ISO-compliant C. This wrapper should significantly reduce the amount of boilerplate code necessary to get OpenCL applications up and running. Future revisions will include yawcl-c++, which would provide the same functionality for users of the C++ language.

What will it do?

It should eventually make it a bit easier to write reasonably fast kernels in OpenCL reasonably quickly. 

Why C rather than C++?

This is starting with C (though I do plan to write a C++ library eventually) because it is far easier for me to implement a coherent program in C than C++ right now. 

What license is this under?

This software is under the GNU LGPL v2 license as described in "notice.txt" in the root directory of the project.
Please keep in mind that support for compiling this project as a library of any sort will come shortly after it is deemed stable and feature complete
enough for proper release.

Mini-disclaimer:

This software is currently in a development stage that could best be described as pre-alpha. It is unstable and unlikely to compile correctly for most configurations. It lacks proper build scripts and is currently just a header library, though that will change later. Do not expect it to have any sort of polish whatsoever.

Please do provide feeback if you insist on using this code regardless!
