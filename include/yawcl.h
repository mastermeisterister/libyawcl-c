/* yawcl.h -- The Yet Another Wrapper for Opencl main header file.
 * It currently doesn't do very much as the project is still very early in development.
 *
 * Copyright (c) 2015 Michael Gohde
 * See "notice.txt" in the root directory of this project for information about licensing (it's LGPL).
 */

#ifndef YAWCL_H
#define YAWCL_H

#include <krnrange.h>
#include <bufwrap.h>
#include <yawclkrn.h>
#include <yawclprg.h>

#endif
