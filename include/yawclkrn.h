/*
 * yawclkrn.h -- Header containing the functions and structures necessary to abstract the operation of an OpenCL kernel.
 * This header will be used extensively by yaclprg.h and yacl.h
 *
 * Copyright (c) 2015 Michael Gohde
 * See "notice.txt" in the root directory of this project for information about licensing (it's LGPL).
 */

#ifndef YAWCLKRN_H
#define YAWCLKRN_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <CL/opencl.h>

#include <bufwrap.h>

typedef struct
{
	cl_kernel krnl;
	cl_context *context;
	cl_command_queue *queue;
	BufferWrapper **bufWrapList;
	bool *bufWrapSharedList;
	size_t numBuffers;
	size_t maxNumBuffers;
	char *kernelName;
} Kernel;

/* This function frees all of the OpenCL and other such resources allocated
 * during the creation of the kernel object passed.
 * 
 * arguments: Kernel *k
 * k -- pointer to kernel struct to destroy.
 * 
 * returns: nothing
 */
extern void freeKernelResources(Kernel *k);

/* This is an internal function that allows the creation of a named cl_kernel from 
 * some given information.
 * 
 * arguments: cl_program *program, cl_command_queue *q, cl_context *newContext, char *kernelName
 * program -- The cl_program to build the kernel from
 * q -- the cl_command_queue to run the kernel on
 * newContext -- the context in which the kernel is to be built
 * kernelName -- the name of the kernel
 * 
 * returns: pointer to a kernel struct
 */
extern Kernel *buildKernelFromProgram(cl_program *program, cl_command_queue *q, cl_context *newContext, char *kernelName);

/* This is an internal function used to expand the buffer arrays used within each 
 * kernel struct.
 * 
 * arguments: Kernel *k
 * k -- pointer to a kernel whose structures need to be expanded.
 * 
 * returns: a bool representing whether or not the operation succeeded.
 */
extern bool expandKernelBufferArray(Kernel *k);

#endif
