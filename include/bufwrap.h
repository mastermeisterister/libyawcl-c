/* bufwrap.h -- a header library wrapper thing that allows programs to use
 * cl_mem objects in a fairly usable struct.
 *
 * Copyright (c) 2015 Michael Gohde
 * See "notice.txt" in the root directory of this project for information about licensing (it's LGPL).
 */

#ifndef BUFWRAP_H
#define BUFWRAP_H

#include <stddef.h>
#include <stdlib.h>
#include <CL/cl.h>

/* You can't extern structures, evidently.*/
typedef struct
{
	cl_mem memBuffer;
	size_t size;
	unsigned long srcPointer;
	void *realSrcPtr;
} BufferWrapper;

/* This function allocates and returns a pointer to a BufferWrapper from the data provided.
 * It should be able to safely convert the pointer's address into an unsigned long.
 * This may cause some problems with 32 bit compilers, which I should look into later.
 *
 * Arguments: cl_mem buffer, size_t size, void *ptr 
 * The first is the buffer on device, the second is the size of the pointer, and the third is a pointer to some data.
 */
extern BufferWrapper *makeBufferWrapper(cl_mem buffer, size_t size, void *ptr);

#endif
