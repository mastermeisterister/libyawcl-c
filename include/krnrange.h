/*
 * krnrange.h -- A header containing functions and structures necessary to somewhat intelligently 
 * run kernels over some range of data.
 *
 * Copyright (c) 2015 Michael Gohde
 * See "notice.txt" in the root directory of this project for information about licensing (it's LGPL).
 */

#ifndef KRNRANGE_H
#define KRNRANGE_H

#include <stddef.h>
#include <stdbool.h>

/* This is the actual (1d) struct itself.
 * Note that it doesn't correspond exactly to the arguments that are passed to enqueueNDRangeKernel*/
typedef struct
{
	size_t workSize;
	size_t workgroupSize;
	size_t remainderSize;
	size_t offsetVal;
	bool calcRemainder;
} KernelRange;

/* This function allocates a new KernelRange on the heap from the given data.
 *
 * Arguments: int workSize, int workgroupSize, bool calcRemainder, bool beSmart
 * workSize -- the total amount of work to do.
 * workgroupSize -- The optimal number of threads per workgroup.
 * calcRemainder -- Whether or not to calculate the remainder.
 * beSmart -- (if true) attempt to run an additional kernel per call to work on the remainder after worksize is
 *            divided by workgroupSize.
 */
extern KernelRange *getKernelRange(int workSize, int workgroupSize, bool calcRemainder, bool beSmart);

#endif
