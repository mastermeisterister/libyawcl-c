# This is a makefile for the entire project.
# It might actually work in some cases!
CC=gcc
CFLAGS=-Wall -Werror -O2 -march=native --std=c99
LDFLAGS=-lOpenCL -L./lib -lyawcl
LIBRARY_PATH=/usr/lib/
INCLUDE_PATH=/usr/include

all: 
	cd src/ && make all && make clean

install:
	cp lib/* $(LIBRARY_PATH)
	cp include/* $(INCLUDE_PATH)

examples:

