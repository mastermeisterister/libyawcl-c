/* test.cl -- A test OpenCL file for my OpenCL wrapper library.
 * Hopefully it will actually work!
 * There's no copyright notice because this code is pretty much common knowledge.
 */

__kernel void vectAdd(__global float *vectA, __global float *vectB, __global float *outVect)
{
	int id=get_global_id(0);

	outVect[id]=vectA[id]+vectB[id];
}

__kernel void addNeigbors(__global float *input, global float *output)
{
	int id=get_global_id(0);
	
	output[id]=input[id]+input[id<<1];
}
