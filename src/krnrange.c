/*
 * krnrange.h -- A header containing functions and structures necessary to somewhat intelligently 
 * run kernels over some range of data.
 *
 * Copyright (c) 2015 Michael Gohde
 * See "notice.txt" in the root directory of this project for information about licensing (it's LGPL).
 */

#ifndef KRNRANGE_C
#define KRNRANGE_C

#include <stddef.h>
#include <stdlib.h>
#include <stdbool.h>

/* This is the actual (1d) struct itself.
 * Note that it doesn't correspond exactly to the arguments that are passed to enqueueNDRangeKernel*/
typedef struct
{
	size_t workSize;
	size_t workgroupSize;
	size_t remainderSize;
	size_t offsetVal;
	bool calcRemainder;
} KernelRange;

KernelRange *getKernelRange(int workSize, int workgroupSize, bool calcRemainder, bool beSmart)
{
	KernelRange *r=(KernelRange*) malloc(sizeof(KernelRange));
	r->calcRemainder=calcRemainder;

	if(calcRemainder)
	{
		r->remainderSize=workSize%workgroupSize;
		r->workgroupSize=workgroupSize;
		
		if(beSmart)
		{
			r->workSize=workSize-r->remainderSize;
			r->offsetVal=r->workSize;
		}
	
		else
		{
			r->workSize=workSize;
			r->offsetVal=r->workSize-r->remainderSize;
		}
	}

	else
	{
		r->remainderSize=0;
		r->workgroupSize=workgroupSize;
		r->workSize=workSize;
		r->offsetVal=0;
	}

	return r;
}

#endif
