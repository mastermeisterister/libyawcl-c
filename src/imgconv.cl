/* This is a simple scaling library thing. */

__kernel void imgScale(__global float *image, __global float *imgOut, __global float *arg)
{
	int id=get_global_id(0);
	int i, j, k;
	float tmpBuffer[27];
	float f, f2, f3;

	id*=4;

	imgOut[id]=0;
	imgOut[id+1]=0;
	imgOut[id+2]=0;
	imgOut[id+3]=255;

	k=0;
	for(i=0;i<3;i++)
	{
		for(j=0;j<3;j++)
		{
			tmpBuffer[(i*3+j)]=image[j+(1600*4*i)+id];//*arg[k];
			tmpBuffer[(i*3+j)+1]=image[j+(1600*4*i)+id+1];//*arg[k];
			tmpBuffer[(i*3+j)+2]=image[j+(1600*4*i)+id+2];//*arg[k];
			k++;
		}
	}

	f=0;
	f2=0;
	f3=0;

	for(i=0;i<9;i++)
	{
		f+=tmpBuffer[(i*3)]*arg[i];
		f2+=tmpBuffer[i*3+1]*arg[i];
		f3+=tmpBuffer[i*3+2]*arg[i]
	}

	imgOut[id]=f;
	imgOut[id+1]=f2;
	imgOut[id+2]=f3;
}
