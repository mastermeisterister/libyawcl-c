/*
 * yawclkrn.h -- Header containing the functions and structures necessary to abstract the operation of an OpenCL kernel.
 * This header will be used extensively by yaclprg.h and yacl.h
 *
 * Copyright (c) 2015 Michael Gohde
 * See "notice.txt" in the root directory of this project for information about licensing (it's LGPL).
 */

#ifndef YAWCLKRN_C
#define YAWCLKRN_C

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include <CL/opencl.h>

#include "../include/bufwrap.h"

typedef struct
{
	cl_kernel krnl;
	cl_context *context;
	cl_command_queue *queue;
	BufferWrapper **bufWrapList;
	bool *bufWrapSharedList;
	size_t numBuffers;
	size_t maxNumBuffers;
	char *kernelName;
} Kernel;

void freeKernelResources(Kernel *k)
{
	BufferWrapper *w;
	size_t i;

	for(i=0;i<k->numBuffers;i++)
	{
		/* Avoid double-freeing shared data, lest all segmentation fault hell break loose.*/
		if(!k->bufWrapSharedList[i])
		{
			w=k->bufWrapList[i];
			clReleaseMemObject(w->memBuffer);
			free(w);
		}
	}

	free(k->bufWrapList);
	free(k->bufWrapSharedList);

	/* The kernel's name and some other various attributes aren't deleted as that is the job of the ClProgram.*/
	clReleaseKernel(k->krnl);
	k->kernelName=NULL;
	k->numBuffers=0;
	k->maxNumBuffers=0;
	k->context=NULL;
	k->queue=NULL;
	k->bufWrapList=NULL;
}

Kernel *buildKernelFromProgram(cl_program *program, cl_command_queue *q, cl_context *newContext, char *kernelName)
{
	int errV;
	Kernel *k=(Kernel*) malloc(sizeof(Kernel));

	k->krnl=clCreateKernel(*program, kernelName, &errV);

	if(errV!=CL_SUCCESS)
	{
		fprintf(stderr, "ERROR: Couldn't build kernel %s. Error code: %d.\n", kernelName, errV);
		exit(1);
	}

	k->queue=q;
	k->context=newContext;
	k->numBuffers=0;
	k->maxNumBuffers=10;
	k->bufWrapList=(BufferWrapper**) malloc(sizeof(BufferWrapper*)*k->maxNumBuffers);
	k->bufWrapSharedList=(bool*) malloc(sizeof(bool)*k->maxNumBuffers);

	return k;
}

bool expandKernelBufferArray(Kernel *k)
{
	size_t i;
	BufferWrapper **list;
	bool *newSharedList;

	/* Note: in future releases, I'll just use realloc. */
	list=(BufferWrapper**)malloc(sizeof(BufferWrapper*)*k->maxNumBuffers+10);
	newSharedList=(bool*)malloc(sizeof(bool)*(k->maxNumBuffers+10));

	if(list==NULL||newSharedList==NULL)
	{
		return false;
	}

	for(i=0;i<k->numBuffers;i++)
	{
		list[i]=k->bufWrapList[i];
		newSharedList[i]=k->bufWrapSharedList[i];
	}

	free(k->bufWrapList);
	k->maxNumBuffers+=10;
	k->bufWrapList=list;

	for(i=k->numBuffers;i<(k->maxNumBuffers-1);i++)
	{
		k->bufWrapSharedList[i]=false;
	}

	return true;
}


#endif
