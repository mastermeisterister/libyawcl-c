/* yawclprg.h -- A header containing the structures and functions necessary to 
 * work with OpenCL programs.
 * 
 * Copyright (c) 2015 Michael Gohde
 * See "notice.txt" in the root directory of this project for information about licensing (it's LGPL).
 */

#ifndef YAWCLPRG_H
#define YAWCLPRG_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <CL/opencl.h>

#include "yawclkrn.h"
#include "krnrange.h"

/* This function returns the function names for a given file. It is used internally by the library, though 
 * it can be useful for other programs (namely a compiler for when binary kernels are supported)
 *
 * arguments: char *file, size_t *numNames, size_t fileSize
 * file -- A character array containing the contents of the source file.
 * numNames -- A pointer that allows the function to return the number of functions found.
 * fileSize -- The size of the array containing the contents of the source file.
 *
 * returns: an array of strings containing the function names.
 */
extern char **getFunctionNames(char *file, size_t *numNames, size_t fileSize);

/* This function returns the size of a file. 
 *
 * Arguments: FILE *f
 * f -- The input file to check the size of.
 *
 * returns: the file size as a size_t.
 */
extern size_t getFileSize(FILE *f);

/* This function reads an entire file as a string.
 * 
 * arguments: char *filename
 * filename -- a string containing the file's name.
 * 
 * returns: a string containing the contents of the file.
 */
extern char *readFile(char *filename);

/* Struct representing the data in a ClProgram.
 * future releases will include a multitude of function pointers in ClProgram to make its 
 * usage similar to that of the planned yawcl-c++ library.
 * 
 * Please note that this struct wasn't extern'd partially so that people's IDEs can
 * detect its members... and for absolutely no reason other than that.
 */
typedef struct
{
	Kernel *kernels;
	size_t numKernels;
	char **kernelNames;
	cl_program p;
	cl_command_queue q;
	cl_device_id device;
	cl_context context;
	cl_platform_id platform;
	char *programSource;
	bool builtFromSource;
	size_t fileSize;
} YawclPrgData;

typedef struct
{
	YawclPrgData *data;
} ClProgram;

/* This function returns a ClProgram struct containing the contents of an 
 * OpenCL 1.1 or later program.
 * 
 * arguments: char *fileName, int deviceType
 * fileName -- the name of the file to read
 * deviceType -- a CL_DEVICE_TYPE representing the type of 
 *               OpenCL device to try to allocate for the kernels.
 * 
 * returns: a pointer to a ClProgram struct.
 */
extern ClProgram *getProgram(char* fileName, int deviceType);

/* This function frees all of the resources allocated while running an OpenCL program.
 * 
 * arguments: ClProgram **p
 * p -- The ClProgram struct to destroy.
 *  
 * returns: nothing
 */
extern void deleteClProgram(ClProgram** p);

/* This function finds and returns a KernelID (which is really just an int) 
 * based on the name of the kernel. This allows users to access and run the kernels 
 * detected and installed by the wrapper.
 * 
 * arguments: ClProgram *p, char *name
 * p -- Pointer to a ClProgram
 * name -- the name of the function (such as "vectorAdd")
 * 
 * returns: int representing the ID of the returned kernel, or -1 if 
 *          the kernel wasn't found.
 */
extern int getKernelIdByName(ClProgram *p, char *name);

/* This is a helper function that allows users and functions implemented by the library
 * to find an allocated buffer by its source pointer. 
 * 
 * arguments: ClProgram *p, int kernelId, void *ptr
 * p -- Pointer to a ClProgram
 * kernelId -- The kernel whose structures the function should search
 * ptr -- the original pointer to whatever object was associated with the memory buffer.
 * 
 * returns: int representing the ID of the buffer to use or -1 if the buffer wasn't found.
 *
 * note: if you're confused by the usage of this function, see the example programs
 * provided in the library's source.
 */
extern int getBufferIdByPointer(ClProgram *p, int kernelId, void *ptr);

/* This function writes data to the corresponding CL_MEM buffer (ie. it transfers data to the device)
 * 
 * arguments: ClProgram *p, int kernelId, int bufferId
 * p -- pointer to a ClProgram
 * kernelId -- The kernel to search for the corresponding data buffer (in future revisions of the library will change this behavior)
 * bufferId -- The buffer to write.
 *
 * returns: bool representing whether or not the data was successfully written.
 */
extern bool writeData(ClProgram *p, int kernelId, int bufferId);

/* This function does the opposite of writeData. It reads the data! 
 * 
 * arguments: ClProgram *p, int kernelId, int bufferId
 * p -- pointer to a ClProgram
 * kernelId -- The kernel to search for the corresponding data buffer.
 * bufferId -- The buffer to read.
 * 
 * returns: bool representing whether or not the data was successfully read.
 */
extern bool readData(ClProgram *p, int kernelId, int bufferId);

/* This function allocates a buffer on an OpenCL compute device, 
 * associates the corresponding buffer with the specified
 * kernel, and writes the data to the device if its access permissions are appropriate.
 * 
 * arguments: ClProgram *p, int kernelId, void *ptr, size_t size, int access
 * p -- pointer to a ClProgram
 * kernelId -- The kernel to associate the data with.
 * ptr -- The source of the data buffer on the host.
 * size -- The size of the data buffer (in bytes) on the host
 * access -- The CL_MEM access permissions for the specified buffer
 *
 * returns: int representing the allocated buffer or -1 if the buffer couldn't be
 *          allocated for any reason.
 */
extern int allocateBuffer(ClProgram *p, int kernelId, void *ptr, size_t size, int access);

/* This funtion allows the user to set the argument of a kernel function at the specified
 * index to a buffer stored in the specified kernel.
 * It is useful for when a cl_mem buffer is allocated on the device and needs to be passed.
 *
 * arguments: ClProgram *p, int kernelId, int argId, int bufferId
 * p -- pointer to a ClProgram
 * kernelId -- The kernel to set the argument of
 * argId -- the index of the argument where the first argument of the kernel function is 0
 * bufferId -- the ID of the buffer to associate with the function.
 * 
 * returns: a bool representing whether or not the function executed correctly.
 */
extern bool setKernelArgToBuffer(ClProgram *p, int kernelId, int argId, int bufferId);

/* This function is the same as setKernelArgToBuffer except it allows constants to be specified to the
 * kernel's function.
 * 
 * arguments: ClProgram *p, int kernelId, int argId, void *ptr, size_t size
 * p -- pointer to a ClProgram
 * kernelId -- The kernel to set the argument of
 * argId -- the index of the argument where the first argument of the kernel function is 0
 * ptr -- pointer to the data to set
 * size -- size of the data to set in bytes
 *
 * returns: a bool reprenting whether or not the function executed correctly.
 */
extern bool setKernelArgToConst(ClProgram *p, int kernelId, int argId, void *ptr, size_t size);

/* This function runs the specified kernel with the specified KernelRange.
 * The KernelRange is a special struct which should make it easier to dispatch 
 * 1-dimensional kernels over some set of data.
 *
 * arguments: ClProgram *p, int kernelId, KernelRange *r
 * p -- pointer to a ClProgram
 * kernelId -- the kernel to run
 * r -- the KernelRange to run the kernel over.
 * 
 * returns: a bool representing whether or not the function executed properly.
 */
extern bool runKernel(ClProgram *p, int kernelId, KernelRange *r);

/* This function associates the data buffer in one kernel to the data buffer pool
 * of the next. It will likely be obsoleted as data management improves in future versions
 * of the library.
 *
 * argumetns: ClProgram *p, int kernelSrc, int kernelDest, int bufferId
 * p -- pointer to a ClProgram
 * kernelSrc -- the source kernel
 * kernelDest -- the destination kernel
 * bufferId -- the ID of the buffer in the source kernel to transfer.
 * 
 * returns: an int representing the ID of the new buffer in the destination.
 *
 * note: once again, direct your attention to the example programs provided with the library
 *       in order to see how this function works.
 */
extern int transferBuffer(ClProgram *p, int kernelSrc, int kernelDest, int bufferId);
#endif
