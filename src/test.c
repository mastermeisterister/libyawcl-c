/*
 * test.c -- A test file for libyawcl-c.
 * This will be subject to change as new configurations and other features are added.
 * 
 * Copyright (c) 2015 Michael Gohde
 * See "notice.txt" in the root directory of this project for information about licensing (it's LGPL).
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "yawcl.c"

#define VECT_SIZE 4096

int main()
{
	int i;
	ClProgram *p;
	float *vect1, *vect2, *vect3;
	int id1, id2, id3, k21, k22;
	int kernel, otherkrn;
	KernelRange *r;

	printf("Allocating vectors...\n");
	vect1=(float*)malloc(sizeof(float)*VECT_SIZE);
	vect2=(float*)malloc(sizeof(float)*VECT_SIZE);
	vect3=(float*)malloc(sizeof(float)*VECT_SIZE);
	
	for(i=0;i<VECT_SIZE;i++)
	{
		vect1[i]=i;
		vect2[i]=VECT_SIZE-i;
		vect3[i]=0;
	}

	printf("Loading program...\n");
	p=getProgram("test.cl", CL_DEVICE_TYPE_GPU);
	otherkrn=getKernelIdByName(p, "addNeigbors");
	kernel=getKernelIdByName(p, "vectAdd");

	printf("Allocating buffers...\n");
	id1=allocateBuffer(p, kernel, vect1, sizeof(float)*VECT_SIZE, CL_MEM_READ_WRITE);
	id2=allocateBuffer(p, kernel, vect2, sizeof(float)*VECT_SIZE, CL_MEM_READ_WRITE);
	id3=allocateBuffer(p, kernel, vect3, sizeof(float)*VECT_SIZE, CL_MEM_READ_WRITE);

	printf("Setting kernel arguments...\n");
	setKernelArgToBuffer(p, kernel, 0, id1);
	setKernelArgToBuffer(p, kernel, 1, id2);
	setKernelArgToBuffer(p, kernel, 2, id3);

	printf("Building kernel range...\n");
	r=getKernelRange(VECT_SIZE, 32, false, false);

	printf("Starting kernel...\n");
	runKernel(p, kernel, r);

	free(r);
	r=getKernelRange(VECT_SIZE/2, 32, false, false); /* Condense the buffer*/
	k21=transferBuffer(p, kernel, otherkrn, id3);
	k22=transferBuffer(p, kernel, otherkrn, id1);

	setKernelArgToBuffer(p, otherkrn, 0, k21);
	setKernelArgToBuffer(p, otherkrn, 1, k22);

	runKernel(p, otherkrn, r);

	printf("Reading back data...\n");
	readData(p, otherkrn, k22);
	printf("Finished reading data\n");
	for(i=0;i<VECT_SIZE/2;i++)
	{
		printf("%f\n", vect1[i]);
		//printf("%f+%f=%f\n", vect1[i], vect2[i], vect3[i]);
	}
	printf("After loop\n");
	free(r);
	/* Now do some cleanup:*/
	deleteClProgram(&p);
	free(vect1);
	free(vect2);
	free(vect3);

	return 0;
}
