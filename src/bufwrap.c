/* bufwrap.h -- a header containing the data and structures necessary to wrap
 * cl_mem objects in a fairly usable struct.
 *
 * Copyright (c) 2015 Michael Gohde
 * See "notice.txt" in the root directory of this project for information about licensing (it's LGPL).
 */

#ifndef BUFWRAP_C
#define BUFWRAP_C

#include <stdbool.h>
#include <stdlib.h>
#include <CL/opencl.h>

typedef struct
{
	cl_mem memBuffer;
	size_t size;
	unsigned long srcPointer;
	void *realSrcPtr;
} BufferWrapper;

/* This function allocates and returns a pointer to a BufferWrapper from the data provided.
 * It should be able to safely convert the pointer's address into an unsigned long.
 * This may cause some problems with 32 bit compilers, which I should look into later.
 */
BufferWrapper *makeBufferWrapper(cl_mem buffr, size_t size, void *ptr)
{
	BufferWrapper *w=(BufferWrapper*) malloc(sizeof(BufferWrapper));

	/* Check if the value is actually valid first. */
	if(w!=NULL)
	{
		w->memBuffer=buffr;
		w->size=size;
		w->srcPointer=(unsigned long)(ptr);
		w->realSrcPtr=ptr; /* Avoid endless casts. If this is later removed, then its removal will
							  be a free efficiency improvement!*/
	}
	return w;
}

#endif
