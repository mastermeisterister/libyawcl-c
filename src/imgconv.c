/* This attempts to apply a transform to an image using OpenCL.
 * It might work!
 */

#include <stdio.h>
#include <stdlib.h>
#include <yawcl.h>
#include <CL/cl.h>
#include <png.h>

int width, height;
png_byte colorType;
png_byte depth;

png_structp image;
png_infop imageInfo;
int numPasses;
png_bytep *pixelRows;

void readImage(char *name)
{
	int i;
	char header[8]; 
	FILE *f;

	f=fopen(name, "rb");
	
	if(f==NULL)
	{
		fprintf(stderr, "ERROR: Couldn't read image file!\n");
		exit(1);
	}

	fread(header, 1, 8, f);

	/* Check if the file read is actually a png image: */
	if(png_sig_cmp(header, 0, 8))
	{
		fprintf(stderr, "Invalid PNG file!\n");
		exit(1);
	}

	image=png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	imageInfo=png_create_info_struct(image);

	if(setjmp(png_jmpbuf(image)))
	{
		fprintf(stderr, "ERROR: Couldn't initialize png image!\n");
		exit(1);
	}

	png_init_io(image, f);
	png_set_sig_bytes(image, 8);
	
	png_read_info(image, imageInfo);
	
	width=png_get_image_width(image, imageInfo);
	height=png_get_image_height(image, imageInfo);
	colorType=png_get_color_type(image, imageInfo);
	depth=png_get_bit_depth(image, imageInfo);

	numPasses=png_set_interlace_handling(image);
    png_read_update_info(image, imageInfo);

    if (setjmp(png_jmpbuf(image)))
    {
		fprintf(stderr, "ERROR: Couldn't read image!\n");
		exit(1);
	}

    pixelRows = (png_bytep*) malloc(sizeof(png_bytep) * height);
	for(i=0;i<height;i++)
	{
		pixelRows[i]=(png_byte*) malloc(png_get_rowbytes(image, imageInfo));
	}

	png_read_image(image, pixelRows);

	fclose(f);
}


void writeImage(char* name)
{
    FILE *f=fopen(name, "wb");
	int i;

    if (f==NULL)
	{
		fprintf(stderr, "ERROR: Couldn't open file for writing!\n");
		exit(1);
	}

	image=png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

	if(image==NULL)
	{
		fprintf(stderr, "ERROR: Couldn't create image write struct!\n");
		fclose(f);
		exit(1);
	}

    imageInfo = png_create_info_struct(image);

	if(image==NULL)
	{
		fprintf(stderr, "ERROR: Couldn't create image info struct!\n");
		fclose(f);
		exit(1);
	}

	if(setjmp(png_jmpbuf(image)))
	{
		fprintf(stderr, "ERROR: Couldn't do some stuff!\n");
		fclose(f);
		exit(1);
	}

    png_init_io(image, f);
	
	if(setjmp(png_jmpbuf(image)))
	{
		fprintf(stderr, "ERROR: Couldn't do some stuff!\n");
		fclose(f);
		exit(1);
	}

	png_set_IHDR(image, imageInfo, width, height,
                 depth, colorType, PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

    png_write_info(image, imageInfo);

	if(setjmp(png_jmpbuf(image)))
	{
		fprintf(stderr, "ERROR: Couldn't do some stuff!\n");
		fclose(f);
		exit(1);
	}

    png_write_image(image, pixelRows);

	if(setjmp(png_jmpbuf(image)))
	{
		fprintf(stderr, "ERROR: Couldn't do some stuff!\n");
		fclose(f);
		exit(1);
	}

    png_write_end(image, NULL);

	for(i=0;i<height;i++)
	{
		free(pixelRows[i]);
	}

	free(pixelRows);
	fclose(f);
}

float *getBufFromImageData()
{
	png_byte *rowPtr;
	png_byte *pixelDat;
	int x, y;
	int bufPos, bufPos2;
	float *buf;
	printf("%d\n", colorType);
	printf("%d %d\n", PNG_COLOR_TYPE_RGBA, PNG_COLOR_TYPE_RGB);
	if(colorType!=PNG_COLOR_TYPE_RGBA)
	{
		fprintf(stderr, "ERROR: Input image not of right type!\n");
		exit(1);
	}

	buf=(float*) malloc(sizeof(float)*width*height*4);
	bufPos=0;

	for(y=0;y<height;y++)
	{
		rowPtr=pixelRows[y];

		for(x=0;x<width;x++)
		{
			pixelDat=&(rowPtr[x*4]);
			buf[bufPos]=pixelDat[0];
			buf[bufPos+1]=pixelDat[1];
			buf[bufPos+2]=pixelDat[2];
			buf[bufPos+3]=pixelDat[3];
			//printf("%f %f %f %f \n", buf[bufPos], buf[bufPos+1], buf[bufPos+2], buf[bufPos+3]);
			bufPos+=4;
		}
	}

	return buf;
}

void clipAndWrite(char *fileName, float *buffer)
{
	png_byte *rowPtr;
	png_byte *pixelDat;
	int x, y;
	int bufPos;

	bufPos=0;

	for(y=0;y<height;y++)
	{
		rowPtr=pixelRows[y];
		
		for(x=0;x<width;x++)
		{
			pixelDat=&(rowPtr[x*4]);

			if(buffer[bufPos]<0)
			{
				buffer[bufPos]*=-1;
			}

			if(buffer[bufPos+1]<0)
			{
				buffer[bufPos+1]*=-1;
			}

			if(buffer[bufPos+2]<0)
			{
				buffer[bufPos+2]*=-1;
			}
			
			if(buffer[bufPos+3]<0)
			{
				buffer[bufPos+3]*=-1;
			}

			if(buffer[bufPos]>255)
			{
				buffer[bufPos]=254;
			}

			if(buffer[bufPos+1]>255)
			{
				buffer[bufPos+1]=254;
			}

			if(buffer[bufPos+2]>255)
			{
				buffer[bufPos+2]=254;
			}
			
			if(buffer[bufPos+3]>255)
			{
				buffer[bufPos+3]=254;
			}

			pixelDat[0]=(png_byte) buffer[bufPos];
			pixelDat[1]=(png_byte) buffer[bufPos+1];
			pixelDat[2]=(png_byte) buffer[bufPos+2];
			pixelDat[3]=(png_byte) buffer[bufPos+3];

			//printf("%d %d %d %d\n", pixelDat[0], pixelDat[1], pixelDat[2], pixelDat[3]);
			//printf("%f %f %f %f \n", buffer[bufPos], buffer[bufPos+1], buffer[bufPos+2], buffer[bufPos+3]);
			bufPos+=4;
		}
	}

	writeImage(fileName);
}

float convMat[]={-1, 1, -1, 1, 0, 1, -1, 1, -1};

int main()
{
	ClProgram *p;
	float *imgBuffer, *outBuffer, arg[1];
	int bufferSize;
	int scaleKernel, imgId, outId, argId;
	KernelRange *r;

	p=getProgram("imgconv.cl", CL_DEVICE_TYPE_GPU);
	arg[0]=1.75;

	readImage("/home/mgohde/Desktop/cdTestImage.png");
	imgBuffer=getBufFromImageData();
	outBuffer=(float*) malloc(sizeof(float)*width*height*4);
	printf("\nGetting kernel...\n");
	//printf("%f\n", arg[1]);

	scaleKernel=getKernelIdByName(p, "imgScale");

	printf("Allocating buffer...\n");
	imgId=allocateBuffer(p, scaleKernel, imgBuffer, sizeof(float)*width*height*4, CL_MEM_READ_WRITE);
	outId=allocateBuffer(p, scaleKernel, outBuffer, sizeof(float)*width*height*4, CL_MEM_READ_WRITE);
	argId=allocateBuffer(p, scaleKernel, convMat, sizeof(float)*9, CL_MEM_READ_WRITE);
	//argId=allocateBuffer(p, scaleKernel, arg, sizeof(float)*1, CL_MEM_READ_WRITE);
	printf("Finished allocating buffers...\n");
	
	setKernelArgToBuffer(p, scaleKernel, 0, imgId);
	setKernelArgToBuffer(p, scaleKernel, 1, outId);
	setKernelArgToBuffer(p, scaleKernel, 2, argId);

	printf("Finished setting kernel args.\n");

	//r=getKernelRange((width)*(height), 64, true, true);
	r=getKernelRange((width-3)*(height-3), 64, true, true);

	printf("Finished getting kernel range.\n");

	runKernel(p, scaleKernel, r);
	printf("Finished running kernel.\n");
	printf("Buffer ids: %d, %d. Kernel ID: %d\n", imgId, argId, scaleKernel);
	readData(p, scaleKernel, outId);
	
	printf("Finished reading data back into buffer.\n");

	clipAndWrite("/home/mgohde/Desktop/out.png", outBuffer);
	printf("Finished clipping program.\n");
	deleteClProgram(&p);
	printf("Finished deleting program.\n");
	free(imgBuffer);

	return 0;
}
