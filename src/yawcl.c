/* yawcl.h -- The Yet Another Wrapper for Opencl main header file.
 * It currently doesn't do very much as the project is still very early in development.
 *
 * Copyright (c) 2015 Michael Gohde
 * See "notice.txt" in the root directory of this project for information about licensing (it's LGPL).
 */

/* This way I can easily compile just one shared library 
 * from the many C source files here. 
 */
#include "krnrange.c"
#include "bufwrap.c"
#include "yawclkrn.c"
#include "yawclprg.c"
