/* yawclprg.h -- A header containing the structures and functions necessary to 
 * work with OpenCL programs.
 * 
 * Copyright (c) 2015 Michael Gohde
 * See "notice.txt" in the root directory of this project for information about licensing (it's LGPL).
 */

#ifndef YAWCLPRG_C
#define YAWCLPRG_C

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <CL/opencl.h>

#include "yawclkrn.h"
#include "krnrange.h"



char **getFunctionNames(char *file, size_t *numNames, size_t fileSize)
{
	char *altFile;//, *otherPtr, *endPos;
	char *strBuffer;//strBuffer[80];
	char **outputList;
	char *tmpStr;
	size_t totalNumFunctions,i;

	totalNumFunctions=0;
	altFile=(char*)malloc(sizeof(char)*fileSize);
	strcpy(altFile, file);

	strBuffer=strtok(altFile, " \n\t");

	/* Scan the file for the number of functions. */
	while(true)
	{
		/* We're finished with this file. */
		if(strBuffer==NULL)
		{
			break; 
		}

		if(strcmp(strBuffer, "__kernel")==0)
		{
			totalNumFunctions++;
		}

		strBuffer=strtok(NULL, " \n\t");
	}

	outputList=(char**) malloc(sizeof(char*)*totalNumFunctions);

	strcpy(altFile, file);
	strBuffer=strtok(altFile, " \n\t");

	i=0;

	/* Why isn't there an infinitely looping structure that allows you to explicitly break out....
     * Aaaand I just described goto. BASIC really does warp the mind.
     */
	while(strBuffer!=NULL)
	{
		if(strcmp(strBuffer, "__kernel")==0)
		{
			strBuffer=strtok(NULL, " \n\t"); /* Get rid of the kernel's return type. */
			strBuffer=strtok(NULL, " (\n\t");

			tmpStr=(char*) malloc(sizeof(char)*strlen(strBuffer));
			strcpy(tmpStr, strBuffer);

			printf("\nFinished adding kernel name: %s\n", tmpStr);

			outputList[i]=tmpStr;
			i++;
		}

		strBuffer=strtok(NULL, " \n\t");
	}

	(*numNames)=totalNumFunctions;	

	return outputList;
}

size_t getFileSize(FILE *f)
{
	size_t curFilePos;
	size_t actualSize;

	curFilePos=ftell(f);

	fseek(f, 0, SEEK_END);
	actualSize=ftell(f);
	
	fseek(f, curFilePos, SEEK_SET);

	return actualSize;
}

/* Yes, there are better ways of reading a file. Especially with binary IO.
 * Are they worth it yet? Not until the feature requests start piling up!
 */
char *readFile(char *filename)
{
	int i;
	char c;
	char *fileBuffer;
	size_t fileSize;
	FILE *f;

	f=fopen(filename, "r");

	if(f==NULL)
	{
		fprintf(stderr, "ERROR: Couldn't open specified OpenCL file.\n");
		exit(1);
	}

	fileSize=getFileSize(f);
	printf("Reported file size: %ld\n", fileSize);
	fileBuffer=(char*) malloc(sizeof(char)*(fileSize+1));
	i=0;

	/* This is genuinely the first do-while loop I've used since Intro to CS. */
	do
	{		
		c=fgetc(f);		
		fileBuffer[i]=c;
		i++;
	} while(c!=EOF);

	fileBuffer[fileSize-1]='\0';

	printf("Full file buffer: %s\n", fileBuffer);
	fclose(f);

	return fileBuffer;
}

typedef struct
{
	Kernel *kernels;
	size_t numKernels;
	char **kernelNames;
	cl_program p;
	cl_command_queue q;
	cl_device_id device;
	cl_context context;
	cl_platform_id platform;
	char *programSource;
	bool builtFromSource;
	size_t fileSize;
} YawclPrgData;

typedef struct
{
	YawclPrgData *data;
} ClProgram;

ClProgram *getProgram(char *fileName, int deviceType)
{
	char *inputFile;
	int i;
	size_t j;
	size_t fileSize;
	ClProgram *p;
	Kernel *k;

	p=(ClProgram*) malloc(sizeof(ClProgram));
	p->data=(YawclPrgData*)malloc(sizeof(YawclPrgData));

	inputFile=readFile(fileName);
	fileSize=strlen(inputFile);
	p->data->kernelNames=getFunctionNames(inputFile, &p->data->numKernels, fileSize);

	printf("Doing OpenCL stuff...");
	/* Now that the basic data shuffling is complete, start the OpenCL:*/
	i=clGetPlatformIDs(1, &p->data->platform, NULL);
	
	if(i!=CL_SUCCESS)
	{
		fprintf(stderr, "ERROR: Can't get CL platform!\n");
		exit(1);
	}

	i=clGetDeviceIDs(p->data->platform, deviceType, 1, &p->data->device, NULL);

	if(i!=CL_SUCCESS)
	{
		fprintf(stderr, "ERROR: Can't get specified CL device! Trying fallback...\n");

		i=clGetDeviceIDs(p->data->platform, CL_DEVICE_TYPE_CPU, 1, &p->data->device, NULL);
		
		if(i!=CL_SUCCESS)
		{
			fprintf(stderr, "ERROR: fallback device failed. Exiting...\n");
			exit(1);
		}
	}

	p->data->context=clCreateContext(0, 1, &p->data->device, NULL, NULL, &i);

	if(i!=CL_SUCCESS)
	{
		fprintf(stderr, "ERROR: Couldn't create CL context!\n");
		exit(1); /* This behavior is likely subject to change as it may not be the best default.*/
	}

	p->data->q=clCreateCommandQueue(p->data->context, p->data->device, 0, &i);
	
	if(i!=CL_SUCCESS)
	{
		fprintf(stderr, "ERROR: Couldn't create CL Command Queue!\n");
		exit(1);
	}

	p->data->p=clCreateProgramWithSource(p->data->context, 1, (const char**) &inputFile, NULL, &i);
	
	if(i!=CL_SUCCESS)
	{
		fprintf(stderr, "ERROR: Couldn't create program from provided CL source.\n");
		fprintf(stderr, "Dump of source:\n%s\n", inputFile);
		exit(1);
	}

	//printf("\ninputFile=%s\n", inputFile);
	clBuildProgram(p->data->p, 0, NULL, NULL, NULL, NULL);

	p->data->kernels=(Kernel*) malloc(sizeof(Kernel)*p->data->numKernels);

	/* Now build every single kernel: */ 
	for(j=0;j<p->data->numKernels;j++)
	{
		k=buildKernelFromProgram(&p->data->p, &p->data->q, &p->data->context, p->data->kernelNames[j]);
		p->data->kernels[j]=*k;
		free(k);
	}

	/* With that mess out of the way, everything should be fine.*/

	return p;
}

void deleteClProgram(ClProgram **p)
{
	size_t i;
	ClProgram *prgm=p[0];

	for(i=0;i<prgm->data->numKernels;i++)
	{
		free(prgm->data->kernelNames[i]);
		freeKernelResources(&prgm->data->kernels[i]);
	}

	free(prgm->data->programSource);
	clReleaseProgram(prgm->data->p);
	clReleaseCommandQueue(prgm->data->q);
	clReleaseContext(prgm->data->context);

	free(prgm);
	prgm=NULL;
}

int getKernelIdByName(ClProgram *p, char *name)
{
	int i;
	
	for(i=0;i<p->data->numKernels;i++)
	{
		if(strcmp(name, p->data->kernelNames[i])==0)
		{
			return i;
		}
	}

	return -1;
}

int getBufferIdByPointer(ClProgram *p, int kernelId, void *ptr)
{
	int i;
	unsigned long ptrVal; /* Use the same means of conversion. */

	ptrVal=(unsigned long)(ptr);
	for(i=0;i<p->data->kernels[kernelId].numBuffers;i++)
	{
		if(p->data->kernels[kernelId].bufWrapList[i]->srcPointer==ptrVal)
		{
			return i;
		}
	}

	return -1;
}

bool writeData(ClProgram *p, int kernelId, int bufferId)
{
	BufferWrapper *w=p->data->kernels[kernelId].bufWrapList[bufferId];
	int retVal;

	retVal=clEnqueueWriteBuffer(p->data->q, w->memBuffer, CL_TRUE, 0, w->size, w->realSrcPtr, 0, NULL, NULL);

	return retVal==CL_SUCCESS;
}

bool readData(ClProgram *p, int kernelId, int bufferId)
{
	BufferWrapper *w=p->data->kernels[kernelId].bufWrapList[bufferId];
	int retVal;
	
	retVal=clEnqueueReadBuffer(p->data->q, w->memBuffer, CL_TRUE, 0, w->size, w->realSrcPtr, 0, NULL, NULL);

	return retVal==CL_SUCCESS;
}

/* If all goes well, then this function should return the ID of the allocated buffer.*/
int allocateBuffer(ClProgram *p, int kernelId, void *ptr, size_t size, int access)
{
	//int errCode;
	cl_mem newBuffer;
	BufferWrapper *w;

	newBuffer=clCreateBuffer(p->data->context, access, size, NULL, NULL);
	w=makeBufferWrapper(newBuffer, size, ptr);

	if(p->data->kernels[kernelId].numBuffers>=p->data->kernels[kernelId].maxNumBuffers)
	{
		expandKernelBufferArray(&(p->data->kernels[kernelId]));
	}

	/* So much dereferencing... so little time... */
	p->data->kernels[kernelId].bufWrapList[p->data->kernels[kernelId].numBuffers]=w;
	
	p->data->kernels[kernelId].numBuffers++;

	/* Automatically write the data if it can be done: */
	if(access==CL_MEM_READ_ONLY||access==CL_MEM_READ_WRITE)
	{
		if(!writeData(p, kernelId, (p->data->kernels[kernelId].numBuffers-1)))
		{
			fprintf(stderr, "ERROR: Couldn't automatically write data to buffer!\n");
		}
	}
	
	return (p->data->kernels[kernelId].numBuffers-1);
}

bool setKernelArgToBuffer(ClProgram *p, int kernelId, int argId, int bufferId)
{
	int errVal;

	/* The following line is kind of terrible. 
	 * For the uninitiated, it calls clSetKernelArg with the kernel, the argument position, 
	 * the size of a cl_mem buffer, and the address of that buffer itself.*/
	errVal=clSetKernelArg(p->data->kernels[kernelId].krnl, argId, sizeof(cl_mem), &(p->data->kernels[kernelId].bufWrapList[bufferId]->memBuffer));

	return errVal==CL_SUCCESS;
}

bool setKernelArgToConst(ClProgram *p, int kernelId, int argId, void *ptr, size_t size)
{
	int errVal;

	errVal=clSetKernelArg(p->data->kernels[kernelId].krnl, argId, size, ptr);

	return errVal==CL_SUCCESS;
}

bool runKernel(ClProgram *p, int kernelId, KernelRange *r)
{
	int errVal;
	
	errVal=clEnqueueNDRangeKernel(p->data->q, p->data->kernels[kernelId].krnl, 1, NULL, &r->workSize, &r->workgroupSize, 0, NULL, NULL);

	if(r->calcRemainder)
	{
		errVal|=clEnqueueNDRangeKernel(p->data->q, p->data->kernels[kernelId].krnl, 1, &r->offsetVal, &r->remainderSize, &r->remainderSize, 0, NULL, NULL);
	}

	return errVal==CL_SUCCESS;
}

int transferBuffer(ClProgram *p, int kernelSrc, int kernelDest, int bufferId)
{
	//int errCode;
	//BufferWrapper *w;

	//newBuffer=clCreateBuffer(p->data->context, access, size, NULL, NULL);
	//w=makeBufferWrapper(newBuffer, size, ptr);

	if(p->data->kernels[kernelSrc].numBuffers>=p->data->kernels[kernelDest].maxNumBuffers)
	{
		expandKernelBufferArray(&(p->data->kernels[kernelDest]));
	}

	/* So much dereferencing... so little time... */
	p->data->kernels[kernelDest].bufWrapList[p->data->kernels[kernelDest].numBuffers]=p->data->kernels[kernelSrc].bufWrapList[bufferId];
	p->data->kernels[kernelDest].bufWrapSharedList[p->data->kernels[kernelDest].numBuffers]=true;
	p->data->kernels[kernelDest].numBuffers++;

	return (p->data->kernels[kernelDest].numBuffers-1);
}
#endif
